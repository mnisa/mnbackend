# mnbackend: A simple rest api service for Simple TODO Application

mnbackend is a web service for simple todo application. It stores data in-memory

## Compiling

### With Docker

- go to project folder ( cd /path/to/mnbackend )
- docker build -t mnbackend .
- docker run -p 127.0.0.1:8080:8080/tcp mnbackend
- go to http://127.0.0.1:8080/todo/status and check ok

## Deployment

### Pipeline

IF you want use pipeline for build or deploy edit bitbucket-pipelines.yml or ...gitlab-ci.yml

## Features

- Health Check
    - GET http://127.0.0.1:8080/todo/status
        - Response Body
        ```
        ok
        ```
        
- List todo items
    - GET http://127.0.0.1:8080/todo
      
        - Response Body
        ```json
        {
          "success": true,
          "error": "",
          "result": [
            {
              "key": 41252352353,
              "issue": "Buy some milk"
            },
            {
              "key": 41252352353,
              "issue": "Buy more some milk"
            },
            {
              "key": 41252352353,
              "issue": "Buy some coffee"
            }
          ] 
        }
        ```
            Response Notes: 
              - key is required (int64)
              - issue a string and required
              - if success is true error is empty, check result
              - if success is false result is empty, check error message

- Add new todo item
    - POST http://127.0.0.1:8080/todo
        - Payload Body
        ```json
        {
          "key": 412542512521,
          "issue": "Buy some milk"
        }
        ```
            Payload Notes: 
              - key is optional (int64), auto generated if blank
              - issue a string and required
      
        - Response Body
        ```json
        {
          "success": true,
          "error": "",
          "result": [
            {
              "key": 41252352353,
              "issue": "Buy some milk"
            },
            {
              "key": 41252352353,
              "issue": "Buy more some milk"
            },
            {
              "key": 41252352353,
              "issue": "Buy some coffee"
            }
          ] 
        }
        ```
            Response Notes: 
              - key is required (int64)
              - issue a string and required
              - if success is true error is empty, check result
              - if success is false result is empty, check error message

## TODO LIST

- Delete a todo item
- Modify a todo item
