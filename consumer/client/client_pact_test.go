package client

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/semihsari/mnbackend/model"

	"net/url"

	"github.com/pact-foundation/pact-go/dsl"
)

var u *url.URL
var client *Client

func TestMain(m *testing.M) {
	var exitCode int

	if os.Getenv("PACT_TEST") != "" {

		fmt.Println("PACT_TEST not null")

		setup()

		exitCode = m.Run()

		if err := pact.WritePact(); err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		pact.Teardown()
	} else {
		exitCode = m.Run()
	}
	os.Exit(exitCode)
}

func TestClientPact_ListTodos(t *testing.T) {
	t.Run("the todo exists", func(t *testing.T) {
		pact.
			AddInteraction().
			Given("Todo exists").
			UponReceiving("A request to todo list").
			WithRequest(request{
				Method: "GET",
				Path:   dsl.S("/todo"),
			}).
			WillRespondWith(dsl.Response{
				Status: 200,
				Body: model.TodosResponse{
					Success: true,
					Result: []*model.Todo{
						{
							Key:   103,
							Issue: "buy some milk",
						},
					},
				},
			})

		err := pact.Verify(func() error {
			todo, err := client.ListTodos()

			if err != nil {
				return fmt.Errorf("an error occurred on list todos %v", err)
			}

			if !todo.Success {
				return fmt.Errorf("list todos not success %v", todo.Error)
			}

			if len(todo.Result) != 1 {
				return fmt.Errorf("invalid todo length expected 1 actual %d", len(todo.Result))
			}

			if todo.Result[0].Key != 103 {
				return fmt.Errorf("wanted todo with ID %d but got %d", 103, todo.Result[0].Key)
			}

			return err
		})

		if err != nil {
			t.Fatalf("Error on Verify: %v", err)
		}
	})
}

func TestClientPact_AddTodo(t *testing.T) {
	t.Run("add todo", func(t *testing.T) {
		pact.
			AddInteraction().
			Given("add todo").
			UponReceiving("A request to create todo").
			WithRequest(request{
				Method: "POST",
				Path:   dsl.S("/todo"),
				Body: model.Todo{
					Key:   108,
					Issue: "buy some milk",
				},
			}).
			WillRespondWith(dsl.Response{
				Status: 200,
				Body: model.TodoResponse{
					Success: true,
					Result: &model.Todo{
						Key:   108,
						Issue: "buy some milk",
					},
				},
			})

		err := pact.Verify(func() error {
			todo, err := client.AddTodo(model.Todo{
				Key:   108,
				Issue: "buy some milk",
			})

			if err != nil {
				return fmt.Errorf("an error occurred on list todos %v", err)
			}

			if !todo.Success {
				return fmt.Errorf("add todo not success %v", todo.Error)
			}

			if todo.Result.Key != 108 {
				return fmt.Errorf("wanted todo with ID %d but got %d", 108, todo.Result.Key)
			}

			return err
		})

		if err != nil {
			t.Fatalf("Error on Verify: %v", err)
		}
	})
}

var pact dsl.Pact

type request = dsl.Request

func setup() {
	pact = createPact()

	pact.Setup(true)

	u, _ = url.Parse(fmt.Sprintf("http://localhost:%d", pact.Server.Port))

	client = &Client{
		BaseURL: u,
	}

}

func createPact() dsl.Pact {
	return dsl.Pact{
		Consumer:                 os.Getenv("CONSUMER_NAME"),
		Provider:                 os.Getenv("PROVIDER_NAME"),
		LogDir:                   os.Getenv("LOG_DIR"),
		PactDir:                  os.Getenv("PACT_DIR"),
		LogLevel:                 "INFO",
		DisableToolValidityCheck: true,
	}
}
