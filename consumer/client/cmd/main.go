package main

import (
	"log"
	"net/url"

	"bitbucket.org/semihsari/mnbackend/model"

	"bitbucket.org/semihsari/mnbackend/consumer/client"
)

func main() {
	u, _ := url.Parse("http://localhost:8080")
	c := &client.Client{
		BaseURL: u,
	}

	todos, err := c.ListTodos()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(todos)

	todo, err := c.AddTodo(model.Todo{
		Key:   105,
		Issue: "buy some milk",
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(todo)
}
