package client

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"net/url"

	"bitbucket.org/semihsari/mnbackend/model"
)

type Client struct {
	BaseURL    *url.URL
	httpClient *http.Client
}

func (c *Client) ListTodos() (*model.TodosResponse, error) {
	req, err := c.newRequest("GET", "/todo", nil)
	if err != nil {
		return nil, err
	}
	response := model.TodosResponse{}
	_, err = c.do(req, &response)

	return &response, err
}

func (c *Client) AddTodo(todo model.Todo) (*model.TodoResponse, error) {
	req, err := c.newRequest("POST", "/todo", todo)
	if err != nil {
		return nil, err
	}
	response := model.TodoResponse{}
	_, err = c.do(req, &response)

	return &response, err
}

func (c *Client) newRequest(method, path string, body interface{}) (*http.Request, error) {
	rel := &url.URL{Path: path}
	u := c.BaseURL.ResolveReference(rel)
	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", "application/json")

	return req, nil
}

func (c *Client) do(req *http.Request, v interface{}) (*http.Response, error) {
	if c.httpClient == nil {
		c.httpClient = http.DefaultClient
	}
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(v)
	return resp, err
}
