package client

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"bitbucket.org/semihsari/mnbackend/model"

	"github.com/stretchr/testify/assert"
)

func TestClientUnit_ListTodos(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.URL.String(), "/todo")
		assert.Equal(t, req.Method, "GET")
		todos, _ := json.Marshal(model.TodosResponse{
			Success: true,
			Error:   "",
			Result: []*model.Todo{
				{
					Key:   101,
					Issue: "buy some milk",
				},
			},
		})
		rw.Write(todos)
	}))
	defer server.Close()

	u, _ := url.Parse(server.URL)
	client := &Client{
		BaseURL: u,
	}
	todos, err := client.ListTodos()
	assert.NoError(t, err)
	assert.True(t, todos.Success)
	assert.Equal(t, int64(101), todos.Result[0].Key)
}

func TestClientUnit_AddTodo(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.URL.String(), "/todo")
		assert.Equal(t, req.Method, "POST")

		body, _ := ioutil.ReadAll(req.Body)

		todo := model.Todo{}
		_ = json.Unmarshal(body, &todo)

		todos, _ := json.Marshal(model.TodoResponse{
			Success: true,
			Error:   "",
			Result:  &todo,
		})
		rw.Write(todos)
	}))
	defer server.Close()

	u, _ := url.Parse(server.URL)
	client := &Client{
		BaseURL: u,
	}
	todos, err := client.AddTodo(model.Todo{
		Key:   102,
		Issue: "buy some milk",
	})
	assert.NoError(t, err)
	assert.True(t, todos.Success)
	assert.Equal(t, int64(102), todos.Result.Key)
	assert.Equal(t, "buy some milk", todos.Result.Issue)
}
