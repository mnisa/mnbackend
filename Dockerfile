FROM golang:1.17 as builder

WORKDIR /go/src/bitbucket.org/semihsari/mnbackend
COPY . .
RUN go mod download
RUN make install
RUN make deploy-consumer
RUN make deploy-provider
RUN make publish
RUN make unit
RUN make consumer
RUN make provider
RUN CGO_ENABLED=0 go build -o mnbackend

FROM alpine:3.10
COPY --from=builder /go/src/bitbucket.org/semihsari/mnbackend/mnbackend /
EXPOSE 8080
CMD ["/mnbackend"]