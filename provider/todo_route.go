package provider

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/semihsari/mnbackend/model"
	"bitbucket.org/semihsari/mnbackend/provider/repository"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
)

type TodoRoute struct {
	Mem repository.IMemory
}

type TodoResponse struct {
	Success bool        `json:"success"`
	Error   string      `json:"error"`
	Result  *model.Todo `json:"result"`
}

type TodosResponse struct {
	Success bool          `json:"success"`
	Error   string        `json:"error"`
	Result  []*model.Todo `json:"result"`
}

func (rs TodoRoute) Routes() chi.Router {
	r := chi.NewRouter()
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))
	r.Get("/status", rs.Status)
	r.Get("/", rs.List)
	r.Post("/", rs.Create)
	return r
}

func (rs TodoRoute) Status(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
	w.Write([]byte("ok"))
}

func (rs TodoRoute) List(w http.ResponseWriter, r *http.Request) {
	todos := rs.Mem.List()
	w.WriteHeader(http.StatusOK)

	response := &TodosResponse{
		Success: true,
		Error:   "",
		Result:  todos,
	}

	json.NewEncoder(w).Encode(response)
}

func (rs TodoRoute) Create(w http.ResponseWriter, r *http.Request) {
	var o model.Todo
	err := json.NewDecoder(r.Body).Decode(&o)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&TodoResponse{
			Error: fmt.Sprintf("istek gövdesi parse edilemedi %v", err),
		})
		return
	}

	todo, err := rs.Mem.Add(&o)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&TodoResponse{
			Error: fmt.Sprintf("iş eklenemedi %v", err),
		})
		return
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(&TodoResponse{
		Success: true,
		Result:  todo,
	})
}
