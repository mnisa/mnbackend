package main

import (
	"log"
	"net/http"

	"bitbucket.org/semihsari/mnbackend/provider"

	"bitbucket.org/semihsari/mnbackend/provider/repository"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func main() {
	mem := repository.NewMemory()

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Mount("/todo", provider.TodoRoute{Mem: mem}.Routes())

	log.Fatal(http.ListenAndServe(":8080", r))
}
