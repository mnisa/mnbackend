package provider

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/semihsari/mnbackend/provider/repository"

	"github.com/go-chi/chi/v5"
)

func TestRouteHealtCheck(t *testing.T) {
	r := chi.NewRouter()
	ds := repository.NewMemory()
	r.Mount("/todo", TodoRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()
	or, resp := testRequest(t, ts, "GET", "/todo/status", nil)
	if resp != `ok` && or.StatusCode == http.StatusOK {
		t.Fatalf("expected %s actual %s", `ok`, resp)
	}
}

func TestRouteList(t *testing.T) {
	r := chi.NewRouter()
	ds := repository.NewMemory()
	r.Mount("/todo", TodoRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()

	expected := `{"success":true,"error":"","result":null}`
	or, resp := testRequest(t, ts, "GET", "/todo", nil)
	if resp != expected && or.StatusCode == http.StatusOK {
		t.Fatalf("expected %s actual %s", expected, resp)
	}
}

func TestRouteCreateWithInvalidJsonRequestBody(t *testing.T) {
	r := chi.NewRouter()
	ds := repository.NewMemory()
	r.Mount("/todo", TodoRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()

	mustContains := "istek gövdesi parse edilemedi"
	or, resp := testRequest(t, ts, "POST", "/todo", bytes.NewBuffer([]byte(`{"key":"1234","issue":"biraz portakal al"`)))
	if !strings.Contains(resp, mustContains) && or.StatusCode != http.StatusBadRequest {
		t.Fatalf("must contains %s actual %s", mustContains, resp)
	}
}

func TestRouteCreateWithSuccessAndDuplicateKeys(t *testing.T) {
	r := chi.NewRouter()
	ds := repository.NewMemory()
	r.Mount("/todo", TodoRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()

	expected := `{"success":true,"error":"","result":{"key":12345,"issue":"biraz portakal al"}}`
	or, resp := testRequest(t, ts, "POST", "/todo", bytes.NewBuffer([]byte(`{"key":12345,"issue":"biraz portakal al"}`)))
	if resp != expected || or.StatusCode != http.StatusCreated {
		t.Fatalf("expected %s actual %s", expected, resp)
	}

	mustContains := "iş eklenemedi"
	or, resp = testRequest(t, ts, "POST", "/todo", bytes.NewBuffer([]byte(`{"key":12345,"issue":"biraz daha portakal al"}`)))
	if !strings.Contains(resp, mustContains) || or.StatusCode != http.StatusBadRequest {
		t.Fatalf("must contains %s actual %s", mustContains, resp)
	}
}

func TestRouteListSortBy(t *testing.T) {
	r := chi.NewRouter()
	ds := repository.NewMemory()
	r.Mount("/todo", TodoRoute{ds}.Routes())
	ts := httptest.NewServer(r)
	defer ts.Close()

	_, _ = testRequest(t, ts, "POST", "/todo", bytes.NewBuffer([]byte(`{"key":7777,"issue":"biraz kiraz al"}`)))
	_, _ = testRequest(t, ts, "POST", "/todo", bytes.NewBuffer([]byte(`{"key":6666,"issue":"biraz kayısı al"}`)))
	_, _ = testRequest(t, ts, "POST", "/todo", bytes.NewBuffer([]byte(`{"key":9999,"issue":"biraz portakal al"}`)))

	expected := `{"success":true,"error":"","result":[{"key":9999,"issue":"biraz portakal al"},{"key":7777,"issue":"biraz kiraz al"},{"key":6666,"issue":"biraz kayısı al"}]}`

	or, resp := testRequest(t, ts, "GET", "/todo", nil)
	if resp != expected && or.StatusCode == http.StatusOK {
		t.Fatalf("expected %s actual %s", expected, resp)
	}
}

func testRequest(t *testing.T, ts *httptest.Server, method, path string, body io.Reader) (*http.Response, string) {
	req, err := http.NewRequest(method, ts.URL+path, body)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}
	defer resp.Body.Close()

	return resp, strings.TrimRight(string(respBody), "\r\n")
}
