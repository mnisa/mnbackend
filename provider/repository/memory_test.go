package repository

import (
	"testing"
	"time"

	"bitbucket.org/semihsari/mnbackend/model"
	"github.com/stretchr/testify/assert"
)

var mem = NewMemory()

func TestMemoryList(t *testing.T) {
	todos := mem.List()
	assert.Nil(t, todos)

	k1 := time.Now().UnixNano()
	_, _ = mem.Add(&model.Todo{
		Key:   k1,
		Issue: "biraz elma al",
	})

	todos = mem.List()
	assert.NotNil(t, todos)
	assert.Len(t, todos, 1)
	assert.Equal(t, todos[0].Key, k1)
	assert.Equal(t, todos[0].Issue, "biraz elma al")
}

func TestMemoryAdd(t *testing.T) {
	k1 := time.Now().UnixNano()
	todo, err := mem.Add(&model.Todo{
		Key:   k1,
		Issue: "biraz süt al",
	})
	assert.Nil(t, err)
	assert.NotNil(t, todo)
	assert.Equal(t, k1, todo.Key)
	assert.Equal(t, "biraz süt al", todo.Issue)

	todo, err = mem.Add(&model.Todo{
		Key:   k1,
		Issue: "biraz daha süt al",
	})
	assert.NotNil(t, err)
	assert.Nil(t, todo)

	todo, err = mem.Add(&model.Todo{
		Issue: "birazcık süt al",
	})
	assert.Nil(t, err)
	assert.NotNil(t, todo)
	assert.Greater(t, todo.Key, int64(0))
	assert.Equal(t, "birazcık süt al", todo.Issue)

	todo, err = mem.Add(todo)
	assert.NotNil(t, err)
	assert.Nil(t, todo)
}
