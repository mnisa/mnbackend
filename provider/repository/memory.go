package repository

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"bitbucket.org/semihsari/mnbackend/model"
)

type IMemory interface {
	List() []*model.Todo
	Add(todo *model.Todo) (*model.Todo, error)
}

type memory struct {
	sync.RWMutex
	data map[int64]*model.Todo
}

func NewMemory() *memory {
	return &memory{
		data: make(map[int64]*model.Todo),
	}
}

func (m *memory) List() []*model.Todo {
	m.RLock()
	defer m.RUnlock()

	var todos []*model.Todo
	for _, t := range m.data {
		todos = append(todos, t)
	}

	sort.Slice(todos, func(i, j int) bool {
		return todos[i].Key > todos[j].Key
	})

	return todos
}

func (m *memory) Add(todo *model.Todo) (*model.Todo, error) {
	m.Lock()
	defer m.Unlock()

	if todo.Key == 0 {
		todo.Key = time.Now().UnixNano()
	}

	if val, ok := m.data[todo.Key]; ok && val != nil {
		return nil, fmt.Errorf("item already exists in cache %d", val.Key)
	}

	m.data[todo.Key] = todo

	return todo, nil
}
