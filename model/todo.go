package model

type Todo struct {
	Key   int64  `json:"key"`
	Issue string `json:"issue"`
}

type TodoResponse struct {
	Success bool   `json:"success"`
	Error   string `json:"error"`
	Result  *Todo  `json:"result"`
}

type TodosResponse struct {
	Success bool    `json:"success"`
	Error   string  `json:"error"`
	Result  []*Todo `json:"result"`
}
