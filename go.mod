module bitbucket.org/semihsari/mnbackend

go 1.15

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/cors v1.2.0
	github.com/kr/pretty v0.3.0
	github.com/pact-foundation/pact-go v1.6.7
	github.com/stretchr/testify v1.7.0
)
