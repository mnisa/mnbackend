include ./make/config.mk

install:
	@if [ ! -d pact/bin ]; then\
		echo "--- Installing Pact CLI dependencies";\
		curl -fsSL https://raw.githubusercontent.com/pact-foundation/pact-ruby-standalone/master/install.sh | bash;\
    fi

run-consumer:
	@go run consumer/client/cmd/main.go

run-provider:
	@go run provider/cmd/todosvc/main.go

deploy-consumer: install
	@echo "--- ✅ Checking if we can deploy consumer"
	@pact-broker can-i-deploy \
		--pacticipant $(CONSUMER_NAME) \
		--broker-base-url ${PACT_BROKER_PROTO}://$(PACT_BROKER_URL) \
		--broker-token $(PACT_BROKER_TOKEN) \
		--latest

deploy-provider: install
	@echo "--- ✅ Checking if we can deploy provider"
	@pact-broker can-i-deploy \
		--pacticipant $(PROVIDER_NAME) \
		--broker-base-url ${PACT_BROKER_PROTO}://$(PACT_BROKER_URL) \
		--broker-token $(PACT_BROKER_TOKEN) \
		--latest

publish: install
	@echo "--- 📝 Publishing Pacts"
	go run consumer/client/pact/publish.go
	@echo
	@echo "Pact contract publishing complete!"
	@echo
	@echo "Head over to $(PACT_BROKER_PROTO)://$(PACT_BROKER_URL) and login with"
	@echo "=> Token: $(PACT_BROKER_TOKEN)"
	@echo "to see your published contracts.	"

unit:
	@echo "--- 🔨Running Unit tests "
	go test bitbucket.org/semihsari/mnbackend/consumer/client -run 'TestClientUnit_ListTodos'
	go test bitbucket.org/semihsari/mnbackend/consumer/client -run 'TestClientUnit_AddTodo'

consumer: export PACT_TEST := true
consumer: install
	@echo "--- 🔨Running Consumer Pact tests "
	go test -count=1 bitbucket.org/semihsari/mnbackend/consumer/client -run 'TestClientPact'

provider: export PACT_TEST := true
provider: install
	@echo "--- 🔨Running Provider Pact tests "
	go test -count=1 -tags=integration bitbucket.org/semihsari/mnbackend/provider -run "TestPactProvider"

.PHONY: install deploy-consumer deploy-provider publish unit consumer provider